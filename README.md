# My Blender Themes

## Flat_Maya-ish
   A dark grey theme with a "flat and square" appearance. It does not use rounded UI elements and is 
   designed around a flat design and medium to gray colors. The viewport uses a Maya-like gradient 
   background.

